# SEEK Asia Job Ads Checkout

A system developed for SEEK Asia's engineering assessment.

## Gitlab repository

Also available on https://gitlab.com/qonn/seekasia-job-ads-checkout

## Requirements

- NodeJS v10+ https://nodejs.org/en/download/
- Yarn https://yarnpkg.com/en/

## Database Design

![Database Diagram](designs/dbdiagram.io.png?raw=true "Database Diagram")

## Why SQLite?

- Easiest to setup, easiest to run, easiest to test.
- Typeorm is fully configurable, swapping to mysql/mssql as easy as changing a property. But for the sake of this demonstration, let's keep it simple.

## Before run backend/frontend or executing tests

Install dependencies by executing the following:

- Open command prompt/terminal
- Change directory to same folder as this readme file
- ```
  cd backend
  yarn
  cd ../frontend
  yarn
  ```

## Running the backend/server

- ```
  cd backend
  yarn start:dev
  ```

## Running the frontend/ui

- First run the backend/server
- ```
  cd frontend
  yarn start:dev
  ```
- Unfinished, can only login, view checkout, add-remove ad unit from and to checkout.
- Available users
  ```
  test
  unilever
  apple
  nike
  ford

  all user's password are 'test'
  ```

## Tests - Server Only

- To run most tests
  ```
  cd backend
  yarn test:e2e
  ```
- Most tests are e2e/functional tests stored in `backend/tests/*`.
- Only one unit test stored in `backend/src/checkout/checkout.modifier.spec.ts`
- To run the single test
  ```
  yarn test
  ```

## Proof of passing tests

![](designs/tests-proof.png?raw=true)

## Running the codegen

- Download https://repo1.maven.org/maven2/io/swagger/swagger-codegen-cli/2.4.7/swagger-codegen-cli-2.4.7.jar
- Run `scripts/swagger-codegen-javascript.bat`
## Credits

- https://docs.nestjs.com
- https://nextjs.org/learn
- https://github.com/swagger-api/swagger-codegen
- https://medium.com/@lupugabriel/using-swagger-codegen-with-reactnative-4493d98cac15
- https://icomoon.io/app