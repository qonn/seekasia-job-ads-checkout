import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { UserService } from '../src/user/user.service';
import { initUserFixtures, login, TestUtils, initAdUnitFixtures, resTextToNumber } from './utils';
import { AdUnitListDto } from '../src/ad-unit/dto/ad-unit-list.dto';
import { ValidationPipe } from '@nestjs/common';
import { AdUnitCreateCmd } from '../src/ad-unit/cmd/ad-unit-create.cmd';
import { AdUnit } from '../src/ad-unit/ad-unit.entity';
import { AdUnitRuleListDto } from '../src/ad-unit-rule/dto/ad-unit-rule-list.dto';
import { AdUnitRuleCreateCmd } from '../src/ad-unit-rule/cmd/ad-unit-rule-create.cmd';
import { AdUnitRuleService } from '../src/ad-unit-rule/ad-unit-rule.service';
import { AdUnitService } from '../src/ad-unit/ad-unit.service';

enum Prefixes {
  List =        "[list]       ",
  Create =      "[create]     ",
  Delete =      "[delete]     ",
  Clear =       "[clear]      ",
  RuleList =    "[rule-list]  ",
  RuleCreate =  "[rule-create]",
  RuleDelete =  "[rule-delete]",
  RuleClear =   "[rule-clear] ",
}

describe('/ad-units', () => {
  let app;
  let baseUrl = '/api/ad-units';
  let token = '';

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();

    app.setGlobalPrefix('api');
    app.useGlobalPipes(new ValidationPipe());

    await app.init();

    await initUserFixtures(app.get(UserService));

    token = await login(app, 'test', 'test');
  });

  afterAll(async () => {
    await app.close();
  });

  /**
   * Ad unit tests
   */

  it(`${Prefixes.List} should return unauthorized without token`, async () => {
    return request(app.getHttpServer())
      .get(`${baseUrl}`)
      .expect(401);
  });

  it(`${Prefixes.Create} should return unauthorized without token`, async () => {
    return request(app.getHttpServer())
      .post(`${baseUrl}`)
      .send({})
      .expect(401);
  });

  it(`${Prefixes.Delete} should return unauthorized without token`, async () => {
    return request(app.getHttpServer())
      .delete(`${baseUrl}/1`)
      .expect(401);
  });

  it(`${Prefixes.Clear} should return unauthorized without token`, async () => {
    return request(app.getHttpServer())
      .delete(`${baseUrl}`)
      .expect(401);
  });

  it(`${Prefixes.List} should return 200 and empty dto`, async () => {
    return request(app.getHttpServer())
      .get(`${baseUrl}`)
      .set('Authorization', `bearer ${token}`)
      .expect(200)
      .then(async res => {
        let dto = res.body as AdUnitListDto;
        expect(Array.isArray(dto.data)).toBe(true);
        expect(dto.data.length).toBe(0);
      });
  });

  it(`${Prefixes.Create} should return 400 on not providing adId`, async () => {
    return request(app.getHttpServer())
      .post(`${baseUrl}`)
      .set('Authorization', `bearer ${token}`)
      .send({})
      .expect(400);
  });

  it(`${Prefixes.Create} should return ad unit generated id`, async () => {
    return request(app.getHttpServer())
      .post(`${baseUrl}`)
      .set('Authorization', `bearer ${token}`)
      .send({ adId: 'test-ad-id' })
      .expect(201)
      .then(async res => {
        expect(JSON.parse(res.text)).toBeGreaterThan(0);
      });
  });

  it(`${Prefixes.List} should return 200 with the newly added ad unit`, async () => {
    return request(app.getHttpServer())
      .get(`${baseUrl}`)
      .set('Authorization', `bearer ${token}`)
      .expect(200)
      .then(async res => {
        let dto = res.body as AdUnitListDto;
        expect(Array.isArray(dto.data)).toBe(true);
        expect(dto.data.length).toBeGreaterThan(0);
        expect(dto.data[0].adId).toBe('test-ad-id');
      });
  });

  it(`${Prefixes.Create} should not be able to create with the same adId`, async () => {
    return request(app.getHttpServer())
      .post(`${baseUrl}`)
      .set('Authorization', `bearer ${token}`)
      .send({ adId: 'test-ad-id' })
      .expect(400);
  });

  it(`${Prefixes.Create} should properly create ad unit with properties`, async () => {
    let cmd: AdUnitCreateCmd = {
      adId: 'test-ad-id-2',
      name: 'Test Ad Id Unit',
      price: 999.99
    };

    await request(app.getHttpServer())
      .post(`${baseUrl}`)
      .set('Authorization', `bearer ${token}`)
      .send(cmd)
      .then(() => Promise.resolve());

    return request(app.getHttpServer())
      .get(`${baseUrl}`)
      .set('Authorization', `bearer ${token}`)
      .expect(200)
      .then(res => {
        let adUnit:AdUnit = res.body.data[1];
        expect(adUnit.id).toBe(2);
        expect(adUnit.name).toBe(cmd.name);
        expect(adUnit.price).toBe(cmd.price);
      });
  });

  it(`${Prefixes.Clear} should successfully clear`, async () => {
    return request(app.getHttpServer())
      .delete(`${baseUrl}`)
      .set('Authorization', `bearer ${token}`)
      .expect(200);
  });

  it(`${Prefixes.List} should return empty after clear`, async () => {
    return request(app.getHttpServer())
      .get(`${baseUrl}`)
      .set('Authorization', `bearer ${token}`)
      .expect(200)
      .then(async res => {
        let dto = res.body as AdUnitListDto;
        expect(Array.isArray(dto.data)).toBe(true);
        expect(dto.data.length).toBe(0);
      });
  });

  it(`${Prefixes.Delete} should be able to delete non-existed ad unit`, async () => {
    return request(app.getHttpServer())
      .delete(`${baseUrl}/100`)
      .set('Authorization', `bearer ${token}`)
      .expect(200)
  });

  it(`${Prefixes.Delete} should properly create and delete ad unit`, async () => {
    let cmd: AdUnitCreateCmd = {
      adId: 'test-ad-id-to-delete',
      name: 'Test Ad Id Unit',
      price: 999.99
    }

    let newlyCreatedId = await request(app.getHttpServer())
      .post(`${baseUrl}`)
      .set('Authorization', `bearer ${token}`)
      .send(cmd)
      .then(res => Promise.resolve(JSON.parse(res.text)));

    await request(app.getHttpServer())
      .delete(`${baseUrl}/${newlyCreatedId}`)
      .set('Authorization', `bearer ${token}`)

    return request(app.getHttpServer())
      .get(`${baseUrl}`)
      .set('Authorization', `bearer ${token}`)
      .expect(200)
      .then(res => {
        let dto = res.body as AdUnitListDto;
        expect(Array.isArray(dto.data)).toBe(true);
        expect(dto.data.length).toBe(0);
      })
  });

  /**
   * Ad unit rule tests
   */

  it(`${Prefixes.RuleCreate} should return unauthorized without token`, async () => {
    return TestUtils.post(app, `${baseUrl}/1/rules`).send({}).expect(401);
  });

  it(`${Prefixes.RuleCreate} should not be able to create without specifying userId`, async () => {
    let testAdUnitRule: Partial<AdUnitRuleCreateCmd> = { adUnitId: 1, payload: "" };
    return TestUtils.post(app, `${baseUrl}/1/rules`, token).send(testAdUnitRule).expect(400);
  });

  it(`${Prefixes.RuleCreate} should not be able to create without specifying adUnitId`, async () => {
    let testAdUnitRule: Partial<AdUnitRuleCreateCmd> = { userId: 1 };
    return TestUtils.post(app, `${baseUrl}/1/rules`, token).send(testAdUnitRule).expect(400);
  });

  it(`${Prefixes.RuleCreate} should be able to create without specifying payload`, async () => {
    let testAdUnit: AdUnitCreateCmd = { adId: 'test-ad-unit-1', name: 'Test Ad Unit', price: 0 };
    let adUnitId = await resTextToNumber(TestUtils.post(app, `${baseUrl}`, token).send(testAdUnit));
    let testAdUnitRule: Partial<AdUnitRuleCreateCmd> = { adUnitId, userId: 1 };
    return TestUtils.post(app, `${baseUrl}/${adUnitId}/rules`, token).send(testAdUnitRule).expect(201);
  });

  it(`${Prefixes.RuleCreate} should return the id of the newly created ad unit rule`, async () => {
    let testAdUnit: AdUnitCreateCmd = { adId: 'test-ad-unit-2', name: 'Test Ad Unit', price: 0 };
    let adUnitId = await resTextToNumber(TestUtils.post(app, `${baseUrl}`, token).send(testAdUnit));
    let testAdUnitRule: AdUnitRuleCreateCmd = { adUnitId, userId: 1, payload: "{}" };
    let res = await TestUtils.post(app, `${baseUrl}/${adUnitId}/rules`, token).send(testAdUnitRule).expect(201);
    expect(JSON.parse(res.text)).toBeGreaterThan(0);
  });

  it(`${Prefixes.RuleList} should return unauthorized without token`, async () => {
    return TestUtils.get(app, `${baseUrl}/1/rules`).expect(401);
  });

  it(`${Prefixes.RuleList} should return empty list`, async () => {
    (await app.get(AdUnitRuleService) as AdUnitRuleService).truncate();
    let res = await TestUtils.get(app, `${baseUrl}/1/rules`, token).expect(200);
    let dto: AdUnitRuleListDto = res.body;
    expect(Array.isArray(dto.data)).toBe(true);
    expect(dto.data.length).toBe(0);
  });

  it(`${Prefixes.RuleList} should return one item on create ad unit rule`, async () => {
    // arrange
    let testAdUnit: AdUnitCreateCmd = { adId: 'test-ad-unit-119', name: 'Test Ad Unit', price: 399.99 };
    let testAdUnitId = await resTextToNumber(TestUtils.post(app, `${baseUrl}`, token).send(testAdUnit));
    let testAdUnitRule: AdUnitRuleCreateCmd = { adUnitId: testAdUnitId, userId: 1, payload: "{}" };
    let res: request.Response = null;

    (await app.get(AdUnitRuleService) as AdUnitRuleService).truncate();

    // act
    res = await TestUtils.post(app, `${baseUrl}/${testAdUnitId}/rules`, token).send(testAdUnitRule).expect(201);
    res = await TestUtils.get(app, `${baseUrl}/${testAdUnitId}/rules`, token).expect(200);

    // assert
    let dto: AdUnitRuleListDto = res.body;
    expect(Array.isArray(dto.data)).toBe(true);
    expect(dto.data.length).toBe(1);
  });

  it(`${Prefixes.RuleDelete} should return unauthorized without token`, async () => {
    return TestUtils.delete(app, `${baseUrl}/1/rules/1`).expect(401);
  });

  it(`${Prefixes.RuleDelete} should return 200 with token`, async () => {
    // arrange
    await initAdUnitFixtures(app.get(AdUnitService))

    // act
    let res1 = await TestUtils.delete(app, `${baseUrl}/1/rules/1`, token).expect(200);

    // assert
    expect(res1.status).toBe(200);
  });

  it(`${Prefixes.RuleDelete} should return 200 on deleting non-existent ad unit rule`, async () => {
    // arrange
    await initAdUnitFixtures(app.get(AdUnitService))

    // act
    let res1 = await TestUtils.delete(app, `${baseUrl}/100/rules/100`, token);

    // assert
    expect(res1.status).toBe(200);
  });

  it(`${Prefixes.RuleClear} should return unauthorized without token`, async () => {
    return TestUtils.delete(app, `${baseUrl}/1/rules`).expect(401);
  });

  it(`${Prefixes.RuleClear} should properly clear with token`, async () => {
    return TestUtils.delete(app, `${baseUrl}/1/rules`, token).expect(200);
  });
});
