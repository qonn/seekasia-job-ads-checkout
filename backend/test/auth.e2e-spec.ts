import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { UserService } from '../src/user/user.service';
import { Repository } from 'typeorm';
import { User } from '../src/user/user.entity';

describe('/auth', () => {
  let app;
  let userService: UserService;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();

    app.setGlobalPrefix('api');

    userService = app.get(UserService);

    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  it('POST /login should return unauthorized on empty data', async () => {
    return request(app.getHttpServer())
      .post('/api/auth/login')
      .send({})
      .expect(401);
  });

  it('POST /login should return token on correct login data', async () => {
    await userService.create({
      username: 'UNILEVER',
      password: 'password'
    });

    return request(app.getHttpServer())
      .post('/api/auth/login')
      .send({ username: 'UNILEVER', password: 'password' })
      .expect(200)
      .then(async res => {
        expect(res.body).toHaveProperty('accessToken');
      });
  });
});
