import { UserService } from "../src/user/user.service";
import * as request from 'supertest';
import { AdUnitService } from "src/ad-unit/ad-unit.service";
import { AdUnitRuleService } from "../src/ad-unit-rule/ad-unit-rule.service";
import { CheckoutService } from "../src/checkout/checkout.service";

function req(app: any, type: string, url: string, token?: string): request.Test {
  let reqSuperTest: request.SuperTest<request.Test> = request(app.getHttpServer());
  let reqTest: request.Test = reqSuperTest[type](url);
  if (token && token.length) reqTest = reqTest.set('Authorization', `bearer ${token}`);
  return reqTest;
}

export class TestUtils {
  public static get(app: any, url: string, token?: string): request.Test {
    return req(app, 'get', url, token);
  }

  public static post(app: any, url: string, token?: string): request.Test {
    return req(app, 'post', url, token);
  }

  public static put(app: any, url: string, token?: string): request.Test {
    return req(app, 'put', url, token);
  }

  public static patch(app: any, url: string, token?: string): request.Test {
    return req(app, 'patch', url, token);
  }

  public static delete(app: any, url: string, token?: string): request.Test {
    return req(app, 'delete', url, token);
  }
}

export async function resTextToNumber(req) {
  return parseInt((await req).text);
}

export async function login(app, username: string, password: string) {
  return request(app.getHttpServer())
    .post('/api/auth/login')
    .send({ username, password })
    .then(res => {
      return Promise.resolve(res.body.accessToken);
    });
}

export async function initUserFixtures(userService: UserService) {
  await userService.truncate();
  await userService.create({ username: 'test', password: 'test' });
  await userService.create({ username: 'testother', password: 'test' });
  await userService.create({ username: 'unilever', password: 'test' });
  await userService.create({ username: 'apple', password: 'test' });
  await userService.create({ username: 'nike', password: 'test' });
  await userService.create({ username: 'ford', password: 'test' });
}

export async function initAdUnitFixtures(adUnitService: AdUnitService) {
  await adUnitService.truncate();
  await adUnitService.create({ adId: 'classic', name: 'Classic Ad', price: 269.99 });
  await adUnitService.create({ adId: 'standout', name: 'Standout Ad', price: 322.99 });
  await adUnitService.create({ adId: 'premium', name: 'Premium Ad', price: 394.99 });
}

export async function initCheckoutScenarioFixtures(checkoutService: CheckoutService, adUnitRuleService: AdUnitRuleService, adUnitService: AdUnitService, userService: UserService) {
  await checkoutService.truncate();
  await adUnitRuleService.truncate();
  await adUnitService.truncate();
  await userService.truncate();

  let userIds = {
    'test':       (await userService.create({ username: 'test', password: 'test' })).id,
    'testother':  (await userService.create({ username: 'testother', password: 'test' })).id,
    'unilever':   (await userService.create({ username: 'unilever', password: 'test' })).id,
    'apple':      (await userService.create({ username: 'apple', password: 'test' })).id,
    'nike':       (await userService.create({ username: 'nike', password: 'test' })).id,
    'ford':       (await userService.create({ username: 'ford', password: 'test' })).id,
  };

  let adUnitIds = {
    'classic':    (await adUnitService.create({ adId: 'classic', name: 'Classic Ad', price: 269.99 })),
    'standout':   (await adUnitService.create({ adId: 'standout', name: 'Standout Ad', price: 322.99 })),
    'premium':    (await adUnitService.create({ adId: 'premium', name: 'Premium Ad', price: 394.99 })),
  };

  let userAdRuleIds = {
    'unilever': [
      (await adUnitRuleService.create({ userId: userIds.unilever, adUnitId: adUnitIds['classic'], payload: `{ "cmd": "xForYDeal", "x": "3", "y": "2" }` }))
    ],

    'apple': [
      (await adUnitRuleService.create({ userId: userIds.apple, adUnitId: adUnitIds['standout'], payload: `{ "cmd": "setPrice", "to": 299.99 }` }))
    ],

    'nike': [
      (await adUnitRuleService.create({ userId: userIds.nike, adUnitId: adUnitIds['premium'], payload: `{ "cmd": "setPriceOnMoreThanXAmountPurchased", "x": "4", "to": 379.99 }` }))
    ],

    'ford': [
      (await adUnitRuleService.create({ userId: userIds.ford, adUnitId: adUnitIds['classic'], payload: `{ "cmd": "xForYDeal", "x": "5", "y": "4" }` })),
      (await adUnitRuleService.create({ userId: userIds.ford, adUnitId: adUnitIds['standout'], payload: `{ "cmd": "setPrice", "to": 309.99 }` })),
      (await adUnitRuleService.create({ userId: userIds.ford, adUnitId: adUnitIds['premium'], payload: `{ "cmd": "setPriceOnMoreThanXAmountPurchased", "x": "3", "to": 389.99 }` })),
    ]
  };

  let userCheckoutItems = {
    'test': [
      (await checkoutService.createItem(userIds['test'], { adUnitId: adUnitIds['classic'] })),
      (await checkoutService.createItem(userIds['test'], { adUnitId: adUnitIds['standout'] })),
      (await checkoutService.createItem(userIds['test'], { adUnitId: adUnitIds['premium'] })),
    ],

    'unilever': [
      (await checkoutService.createItem(userIds['unilever'], { adUnitId: adUnitIds['classic'] })),
      (await checkoutService.createItem(userIds['unilever'], { adUnitId: adUnitIds['classic'] })),
      (await checkoutService.createItem(userIds['unilever'], { adUnitId: adUnitIds['classic'] })),
      (await checkoutService.createItem(userIds['unilever'], { adUnitId: adUnitIds['premium'] })),
    ],

    'apple': [
      (await checkoutService.createItem(userIds['apple'], { adUnitId: adUnitIds['standout'] })),
      (await checkoutService.createItem(userIds['apple'], { adUnitId: adUnitIds['standout'] })),
      (await checkoutService.createItem(userIds['apple'], { adUnitId: adUnitIds['standout'] })),
      (await checkoutService.createItem(userIds['apple'], { adUnitId: adUnitIds['premium'] })),
    ],

    'nike': [
      (await checkoutService.createItem(userIds['nike'], { adUnitId: adUnitIds['premium'] })),
      (await checkoutService.createItem(userIds['nike'], { adUnitId: adUnitIds['premium'] })),
      (await checkoutService.createItem(userIds['nike'], { adUnitId: adUnitIds['premium'] })),
      (await checkoutService.createItem(userIds['nike'], { adUnitId: adUnitIds['premium'] })),
    ],

    'ford': [
      (await checkoutService.createItem(userIds['ford'], { adUnitId: adUnitIds['classic'] })),
      (await checkoutService.createItem(userIds['ford'], { adUnitId: adUnitIds['classic'] })),
      (await checkoutService.createItem(userIds['ford'], { adUnitId: adUnitIds['classic'] })),
      (await checkoutService.createItem(userIds['ford'], { adUnitId: adUnitIds['classic'] })),
      (await checkoutService.createItem(userIds['ford'], { adUnitId: adUnitIds['classic'] })),
      (await checkoutService.createItem(userIds['ford'], { adUnitId: adUnitIds['standout'] })),
      (await checkoutService.createItem(userIds['ford'], { adUnitId: adUnitIds['standout'] })),
      (await checkoutService.createItem(userIds['ford'], { adUnitId: adUnitIds['standout'] })),
      (await checkoutService.createItem(userIds['ford'], { adUnitId: adUnitIds['standout'] })),
      (await checkoutService.createItem(userIds['ford'], { adUnitId: adUnitIds['standout'] })),
      (await checkoutService.createItem(userIds['ford'], { adUnitId: adUnitIds['premium'] })),
      (await checkoutService.createItem(userIds['ford'], { adUnitId: adUnitIds['premium'] })),
      (await checkoutService.createItem(userIds['ford'], { adUnitId: adUnitIds['premium'] })),
      (await checkoutService.createItem(userIds['ford'], { adUnitId: adUnitIds['premium'] })),
      (await checkoutService.createItem(userIds['ford'], { adUnitId: adUnitIds['premium'] })),
    ],
  }

  return {
    userIds,
    adUnitIds,
    userAdRuleIds,
    userCheckoutItems,
  }
}