import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../src/app.module';
import { UserService } from '../src/user/user.service';
import { initUserFixtures, login, TestUtils, initCheckoutScenarioFixtures } from './utils';
import { ValidationPipe } from '@nestjs/common';
import { CheckoutService } from '../src/checkout/checkout.service';
import * as request from 'supertest';
import { AdUnitCreateCmd } from '../src/ad-unit/cmd/ad-unit-create.cmd';
import { AdUnitService } from '../src/ad-unit/ad-unit.service';
import { CheckoutItemCreateCmd } from '../src/checkout/cmd/checkout-item-create.cmd';
import { AdUnitRuleService } from '../src/ad-unit-rule/ad-unit-rule.service';

let app;
let token = '';
let baseUrl = '/api/checkout';

describe(baseUrl, () => {

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();

    app.setGlobalPrefix('api');
    app.useGlobalPipes(new ValidationPipe());

    await app.init();

    await initUserFixtures(app.get(UserService));

    token = await login(app, 'test', 'test');
  });

  afterAll(async () => {
    await app.close();
  });

  describe(`POST ${baseUrl}`, () => {
    it('should return 401 if unauthorized', async () => {
      return TestUtils.post(app, `${baseUrl}`).expect(401);
    });

    it('should return new checkout item id on item creation', async () => {
      // arrange
      let item = { adUnitId: 1 };
      let adUnitCreateCmd: AdUnitCreateCmd = { adId: 'test', name: 'Test', price: 399 };
      let ress: request.Response[] = [];
      await (app.get(CheckoutService) as CheckoutService).truncate();
      await (app.get(AdUnitService) as AdUnitService).truncate();
      item.adUnitId = await (app.get(AdUnitService) as AdUnitService).create(adUnitCreateCmd);
      // act
      ress.push(await TestUtils.post(app, `${baseUrl}`, token).send(item));
      // assert
      expect(ress[0].status).toBe(201);
      expect(JSON.parse(ress[0].text)).toBe(1);
    });
  });

  describe(`DELETE ${baseUrl}`, () => {
    it('should return 401 if unauthorized', async () => {
      return TestUtils.delete(app, `${baseUrl}`).expect(401);
    });

    it('should return 200 if authorized', async () => {
      return TestUtils.delete(app, `${baseUrl}`, token).expect(200);
    });
  });

  describe(`DELETE ${baseUrl}/:itemId`, () => {
    it('should return 401 if unauthorized', async () => {
      return TestUtils.delete(app, `${baseUrl}/1`).expect(401);
    });

    it('should return 400 if authorized', async () => {
      //arrange
      await (app.get(CheckoutService) as CheckoutService).truncate();
      await (app.get(AdUnitService) as AdUnitService).truncate();
      // init some data
      let adUnitId = await (app.get(AdUnitService) as AdUnitService).create({adId: 'test', name: 'Test Ad', price: 399 });
      // checkout item to user test
      let checkoutItemid = await TestUtils.post(app, `${baseUrl}`, token).send({ adUnitId } as CheckoutItemCreateCmd);
      //act
      let res = await TestUtils.delete(app, `${baseUrl}/${checkoutItemid}`, token);
      //assert
      expect(res.status).toBe(400);
    });

    it('should return 401 on deleting checkout item that is not belong to user\'s checkout', async () => {
      //arrange
      await (app.get(CheckoutService) as CheckoutService).truncate();
      await (app.get(AdUnitService) as AdUnitService).truncate();
      // create test ad
      let adUnitId = await (app.get(AdUnitService) as AdUnitService).create({adId: 'test', name: 'Test Ad', price: 399 });
      // login userother from fixtures
      let tokenOther = await login(app, 'testother', 'test');
      // checkout same item on both users
      await TestUtils.post(app, `${baseUrl}`, token).send({ adUnitId } as CheckoutItemCreateCmd);
      let { text: checkoutItemId2 } = await TestUtils.post(app, `${baseUrl}`, tokenOther).send({ adUnitId } as CheckoutItemCreateCmd);

      //act
      let res = await TestUtils.delete(app, `${baseUrl}/${checkoutItemId2}`, token);

      //assert
      expect(res.status).toBe(401);
    });
  });

  describe(`GET ${baseUrl}`, () => {
    it('should return 401 if unauthorized', async () => {
      return TestUtils.get(app, `${baseUrl}`).expect(401);
    });

    it('should return 200 if authorized', async () => {
      return TestUtils.get(app, `${baseUrl}`, token).expect(200);
    });

    it('should auto create and return checkout if not yet created', async () => {
      //arrange
      await (app.get(CheckoutService) as CheckoutService).truncate();
      await (app.get(AdUnitService) as AdUnitService).truncate();

      //act
      let res = await TestUtils.get(app, `${baseUrl}`, token);

      //assert
      expect(res.status).toBe(200);
      expect(res.body).toEqual({ items: [], total: 0 });
    });

    it('should return checkout with increased items total on checkout creation', async () => {
      //arrange
      await (app.get(CheckoutService) as CheckoutService).truncate();
      await (app.get(AdUnitService) as AdUnitService).truncate();
      let previousLength = (await TestUtils.get(app, `${baseUrl}`, token)).body.items.length;
      let adUnitId = await (app.get(AdUnitService) as AdUnitService).create({adId: 'test', name: 'Test Ad', price: 399 });
      await TestUtils.post(app, `${baseUrl}`, token).send({ adUnitId } as CheckoutItemCreateCmd);

      //act
      let res = await TestUtils.get(app, `${baseUrl}`, token);

      //assert
      expect(res.status).toBe(200);
      expect(res.body.items.length).toBeGreaterThan(previousLength);
    });

    it('should return checkout with decreased items total on deleting checkout item', async () => {
      //arrange
      await (app.get(CheckoutService) as CheckoutService).truncate();
      await (app.get(AdUnitService) as AdUnitService).truncate();
      // initialize some checkout creation and deletion
      let adUnitId = await (app.get(AdUnitService) as AdUnitService).create({adId: 'test', name: 'Test Ad', price: 399 });
      await TestUtils.post(app, `${baseUrl}`, token).send({ adUnitId } as CheckoutItemCreateCmd);
      await TestUtils.post(app, `${baseUrl}`, token).send({ adUnitId } as CheckoutItemCreateCmd);
      let { text: checkoutItemId } = await TestUtils.post(app, `${baseUrl}`, token).send({ adUnitId } as CheckoutItemCreateCmd);
      let previousLength = (await TestUtils.get(app, `${baseUrl}`, token)).body.items.length;
      await TestUtils.delete(app, `${baseUrl}/${checkoutItemId}`, token);

      //act
      let res = await TestUtils.get(app, `${baseUrl}`, token);

      //assert
      expect(res.status).toBe(200);
      expect(res.body.items.length).toBeLessThan(previousLength);
    });

    it('should return checkout with empty items on clearing checkout', async () => {
      //arrange
      await TestUtils.delete(app, `${baseUrl}`, token);

      //act
      let res = await TestUtils.get(app, `${baseUrl}`, token);

      //assert
      expect(res.status).toBe(200);
      expect(res.body.items.length).toBe(0);
    });

    it('should return checkout that satisfy SEEK Asia\'s scenario #1', async () => {
      //arrange
      await initCheckoutScenarioFixtures(
        app.get(CheckoutService),
        app.get(AdUnitRuleService),
        app.get(AdUnitService),
        app.get(UserService),
      );

      let userToken = await login(app, 'test', 'test');

      //act
      let res = await TestUtils.get(app, `${baseUrl}`, userToken);

      //assert
      expect(res.status).toBe(200);
      expect((res.body.total).toFixed(2)).toBe('987.97');
    });

    it('should return checkout that satisfy SEEK Asia\'s scenario #2', async () => {
      //arrange
      //reuse previous fixtures
      //login
      let userToken = await login(app, 'unilever', 'test');

      //act
      let res = await TestUtils.get(app, `${baseUrl}`, userToken);

      //assert
      expect(res.status).toBe(200);
      expect((res.body.total).toFixed(2)).toBe('934.97');
    });

    it('should return checkout that satisfy SEEK Asia\'s scenario #3', async () => {
      //arrange
      //reuse previous fixtures
      //login
      let userToken = await login(app, 'apple', 'test');

      //act
      let res = await TestUtils.get(app, `${baseUrl}`, userToken);

      //assert
      expect(res.status).toBe(200);
      expect((res.body.total).toFixed(2)).toBe('1294.96');
    });

    it('should return checkout that satisfy SEEK Asia\'s scenario #4', async () => {
      //arrange
      //reuse previous fixtures
      //login
      let userToken = await login(app, 'nike', 'test');

      //act
      let res = await TestUtils.get(app, `${baseUrl}`, userToken);

      //assert
      expect(res.status).toBe(200);
      expect((res.body.total).toFixed(2)).toBe('1519.96');
    });

    it('should return checkout that satisfy SEEK Asia\'s scenario #5', async () => {
      //arrange
      //reuse previous fixtures
      //login
      let userToken = await login(app, 'ford', 'test');

      //act
      let res = await TestUtils.get(app, `${baseUrl}`, userToken);

      //assert
      expect(res.status).toBe(200);
      expect((res.body.total).toFixed(2)).toBe('4579.86');
    });
  });
});
