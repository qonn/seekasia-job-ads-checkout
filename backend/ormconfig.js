module.exports = {
  "type": "sqlite",
  "database": process.env.NODE_ENV === 'test' ? "test.sqlite" : "data.sqlite",
  "entities": [process.env.NODE_ENV === 'test' ? __dirname + '/../**/**.entity.ts' : __dirname + '/../**/**.entity.js' ],
  "synchronize": process.env.NODE_ENV === 'test' ? true: false,
  "dropSchema": process.env.NODE_ENV === 'test' ? true : false,
  // "logging": true
}