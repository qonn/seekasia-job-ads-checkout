import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { AdUnitRule } from "../ad-unit-rule/ad-unit-rule.entity";
import { Checkout } from '../checkout/checkout.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  username: string;

  @Column()
  password: string;

  @OneToMany(type => AdUnitRule, adUnitRule => adUnitRule.user)
  adUnitRules: AdUnitRule[];

  @OneToMany(type => Checkout, checkout => checkout.user)
  checkouts: Checkout[];
}