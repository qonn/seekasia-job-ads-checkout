import * as bcrypt from 'bcrypt';
import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { UserCreateDto } from './dto/user-create.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  public async getById(id: number): Promise<User> {
    return this.userRepository.findOne(id);
  }

  public async findOne(username: string): Promise<User> {
    return this.userRepository.findOne({ where: { username } });
  }

  public async findAll(): Promise<User[]> {
    return this.userRepository.find();
  }

  public async create(user: UserCreateDto): Promise<User> {
    user.password = await bcrypt.hash(user.password, 10);

    try {
      return await this.userRepository.save(user);
    } catch (err) {
      throw new BadRequestException(err);
    }
  }

  async truncate() {
    return await this.userRepository.clear();
  }
}