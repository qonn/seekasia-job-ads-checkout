import { ApiModelProperty } from "@nestjs/swagger";

export class UserCreateDto {
  @ApiModelProperty() username: string;
  @ApiModelProperty() password: string;
}