import { Controller, Get, Post, UseGuards, Request } from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth, ApiOperation } from '@nestjs/swagger';
import { User } from './user.entity';
import { UserService } from './user.service';
import { AuthGuard } from '@nestjs/passport';

@ApiUseTags('Users')
@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  @ApiOperation({ title: "List all users" })
  async list(): Promise<User[]> {
    return this.userService.findAll();
  }

  @Get('me')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ title: "Show myself" })
  getProfile(@Request() req): Promise<any> {
    return req.user;
  }
}
