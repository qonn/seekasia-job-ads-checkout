import { Module } from '@nestjs/common';
import { AdUnitController } from './ad-unit.controller';
import { AdUnitService } from './ad-unit.service';
import { AdUnit } from './ad-unit.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdUnitRuleModule } from '../ad-unit-rule/ad-unit-rule.module';

@Module({
  imports: [AdUnitRuleModule, TypeOrmModule.forFeature([AdUnit])],
  controllers: [AdUnitController],
  providers: [AdUnitService],
  exports: [AdUnitService]
})
export class AdUnitModule {}
