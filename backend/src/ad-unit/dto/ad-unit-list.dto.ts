import { AdUnitListItemDto } from "./ad-unit-list-item.dto";

export class AdUnitListDto {
  data: AdUnitListItemDto[] = [];
}