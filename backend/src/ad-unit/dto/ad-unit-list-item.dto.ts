import { ApiModelProperty } from "@nestjs/swagger";

export class AdUnitListItemDto {
  @ApiModelProperty() id: number;
  @ApiModelProperty() adId: string;
  @ApiModelProperty() name: string;
  @ApiModelProperty() price: number;
}