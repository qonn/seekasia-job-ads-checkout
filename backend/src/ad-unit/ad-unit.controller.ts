import {
  Controller,
  Post,
  UseGuards,
  Delete,
  Param,
  Get,
  Body,
  Req,
  Patch,
} from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth, ApiOperation } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { AdUnitListDto } from './dto/ad-unit-list.dto';
import { AdUnitCreateCmd } from './cmd/ad-unit-create.cmd';
import { AdUnitService } from './ad-unit.service';
import { AdUnitRuleCreateCmd } from '../ad-unit-rule/cmd/ad-unit-rule-create.cmd';
import { AdUnitRuleService } from '../ad-unit-rule/ad-unit-rule.service';
import { AdUnitRuleListQuery } from '../ad-unit-rule/query/ad-unit-rule-list.query';
import { AdUnitRuleUpdateCmd } from '../ad-unit-rule/cmd/ad-unit-rule-update.cmd';

@ApiUseTags('Ad Units')
@ApiBearerAuth()
@Controller('ad-units')
export class AdUnitController {
  constructor(
    private readonly adUnitService: AdUnitService,
    private readonly adUnitRuleService: AdUnitRuleService,
  ) {}

  /**
   * Ad units endopints
   */

  @Get()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ title: 'List all ad units available' })
  public async list(): Promise<AdUnitListDto> {
    return await this.adUnitService.list();
  }

  @Post()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ title: 'Create new ad unit' })
  public async create(@Body() cmd: AdUnitCreateCmd) {
    return await this.adUnitService.create(cmd);
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ title: 'Delete an ad unit' })
  public async delete(@Param('id') id: number) {
    return await this.adUnitService.delete(id);
  }

  @Delete()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ title: 'Clear all ad units' })
  public async clear() {
    return await this.adUnitService.clear();
  }

  /**
   * Ad unit rules endpoints
   */

  @Get(':id/rules')
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ title: 'List all ad unit rules' })
  public async listRule(
    @Param('id') adUnitId: number,
    @Body() query: AdUnitRuleListQuery,
  ) {
    query.adUnitId = adUnitId;
    return await this.adUnitRuleService.list(query);
  }

  @Post(':id/rules')
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ title: 'Create new ad unit rule' })
  public async createRule(
    @Param('id') adUnitId: number,
    @Body() cmd: AdUnitRuleCreateCmd,
  ) {
    cmd.adUnitId = adUnitId;
    return await this.adUnitRuleService.create(cmd);
  }

  @Post(':id/rules/:ruleId')
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ title: 'Update ad unit rule by rule id' })
  public async update(
    @Param('ruleId') adUnitRuleId: number,
    @Body() cmd: AdUnitRuleUpdateCmd,
  ) {
    return await this.adUnitRuleService.update({ ...cmd, ...{ id: adUnitRuleId } });
  }

  @Delete(':id/rules/:ruleId')
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ title: 'Delete a single ad unit rule by rule id' })
  public async deleteRule(
    @Param('ruleId') adUnitRuleId: number
  ) {
    return await this.adUnitRuleService.delete({ id: adUnitRuleId });
  }

  @Delete(':id/rules')
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ title: 'Clear all ad unit rules for the ad unit' })
  public async clearRules(
    @Param('id') adUnitId: number,
  ) {
    return await this.adUnitRuleService.clear({ adUnitId });
  }
}
