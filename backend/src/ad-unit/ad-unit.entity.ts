import { Entity, PrimaryGeneratedColumn, Column, OneToMany, Unique } from "typeorm";
import { AdUnitRule } from "../ad-unit-rule/ad-unit-rule.entity";

@Entity()
@Unique(['adId'])
export class AdUnit {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  adId: string;

  @Column({ default: "" })
  name: string;

  @Column({ default: 0 })
  price: number;

  @OneToMany(type => AdUnitRule, adUnitRule => adUnitRule.adUnit)
  rules: AdUnitRule[];
}

