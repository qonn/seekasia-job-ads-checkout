import { ApiModelProperty } from "@nestjs/swagger";
import { IsNotEmpty } from 'class-validator';

export class AdUnitCreateCmd {
  @IsNotEmpty()
  @ApiModelProperty()
  adId: string;

  @ApiModelProperty()
  name?: string = "";

  @ApiModelProperty()
  price?: number = 0;
}