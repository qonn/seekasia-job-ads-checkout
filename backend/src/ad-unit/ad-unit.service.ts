import { Injectable, BadRequestException } from '@nestjs/common';
import { AdUnitCreateCmd } from './cmd/ad-unit-create.cmd';
import { AdUnit } from './ad-unit.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { AdUnitListDto } from './dto/ad-unit-list.dto';

@Injectable()
export class AdUnitService {
  constructor(
    @InjectRepository(AdUnit)
    private readonly adUnitRepository: Repository<AdUnit>,
  ) {}

  async list() {
    let dto = new AdUnitListDto();
    dto.data = await this.adUnitRepository.find();
    return dto;
  }

  async create(cmd: AdUnitCreateCmd) {
    if (await this.has(cmd.adId)) {
      throw new BadRequestException(
        'Ad unit with that adId has already been added.',
      );
    }

    let adUnit = this.adUnitRepository.create(cmd);

    adUnit = await this.adUnitRepository.save(adUnit);

    return adUnit.id;
  }

  async clear() {
    return await this.adUnitRepository.clear();
  }

  async has(adId: string) {
    return (await this.adUnitRepository.count({ where: { adId } })) > 0;
  }

  async delete(id: number) {
    return await this.adUnitRepository.delete({ id });
  }

  async getById(id: number) {
    return await this.adUnitRepository.findOne(id);
  }

  async truncate() {
    return await this.adUnitRepository.clear();
  }
}
