export class AdUnitRuleListItemDto {
  id: number;
  userId: number;
  username: string;
}