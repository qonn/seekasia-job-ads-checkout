import { AdUnitRuleListItemDto } from "./ad-unit-rule-list-item.dto";

export class AdUnitRuleListDto {
  data: AdUnitRuleListItemDto[] = [];
}