import { Injectable } from '@nestjs/common';
import { AdUnitRuleListQuery } from './query/ad-unit-rule-list.query';
import { AdUnitRuleCreateCmd } from './cmd/ad-unit-rule-create.cmd';
import { InjectRepository } from '@nestjs/typeorm';
import { AdUnitRule } from './ad-unit-rule.entity';
import { Repository } from 'typeorm';
import { AdUnitRuleUpdateCmd } from './cmd/ad-unit-rule-update.cmd';
import { AdUnitRuleDeleteByIdCmd } from './cmd/ad-unit-rule-delete-by-id.cmd';
import { AdUnitRuleClearCmd } from './cmd/ad-unit-rule-clear.cmd';
import { AdUnitRuleListDto } from './dto/ad-unit-rule-list.dto';
import { AdUnitRuleListItemDto } from './dto/ad-unit-rule-list-item.dto';
import { User } from '../user/user.entity';
import { AdUnit } from '../ad-unit/ad-unit.entity';

@Injectable()
export class AdUnitRuleService {
  constructor(
    @InjectRepository(AdUnitRule)
    private readonly adUnitRuleRepository: Repository<AdUnitRule>,
  ) {}

  async list(query: AdUnitRuleListQuery) {
    let adUnitRuleListItemDtos: AdUnitRuleListItemDto[] = await this
      .adUnitRuleRepository
      .createQueryBuilder("adUnitRule")
      .leftJoin(User, "user", "user.id = adUnitRule.userId")
      .leftJoin(AdUnit, "adUnit", "adUnit.id = adUnitRule.adUnitId")
      .select([
        "adUnitRule.id",
        "user.id as userId",
        "user.username"
      ])
      .getRawMany();

    let listDto: AdUnitRuleListDto = {
      data: adUnitRuleListItemDtos
    }

    return listDto;
  }

  async create(cmd: AdUnitRuleCreateCmd) {
    let adUnitRule = await this.adUnitRuleRepository.save({ ...cmd, adUnit: { id: cmd.adUnitId }, user: { id: cmd.userId } });
    return adUnitRule.id;
  }

  async update(cmd: AdUnitRuleUpdateCmd) {
    return await this.adUnitRuleRepository.update(
      { adUnit: { id: cmd.id } },
      { payload: cmd.payload },
    );
  }

  async delete(cmd: AdUnitRuleDeleteByIdCmd) {
    return await this.adUnitRuleRepository.delete(cmd.id);
  }

  async clear(cmd: AdUnitRuleClearCmd) {
    return await this.adUnitRuleRepository.delete({
      adUnit: { id: cmd.adUnitId },
      ...(cmd.userId ? { user: { id: cmd.userId } } : {}),
    });
  }

  async findByUserId(id: number) {
    return await this.adUnitRuleRepository.find({ where: { user: { id } }, relations: ['adUnit'] });
  }

  async truncate() {
    return await this.adUnitRuleRepository.clear();
  }
}
