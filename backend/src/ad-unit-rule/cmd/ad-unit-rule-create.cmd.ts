import { ApiModelProperty, ApiModelPropertyOptional } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class AdUnitRuleCreateCmd {
  @IsNotEmpty()
  @ApiModelProperty()
  public adUnitId: number;

  @IsNotEmpty()
  @ApiModelProperty()
  public userId: number;

  @ApiModelPropertyOptional()
  public payload?: string = "{}";
}