import { ApiModelProperty } from "@nestjs/swagger";

export class AdUnitRuleDeleteByIdCmd {
  @ApiModelProperty() id: number;
}