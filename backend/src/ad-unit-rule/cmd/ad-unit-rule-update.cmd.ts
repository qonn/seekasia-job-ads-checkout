import { ApiModelProperty, ApiModelPropertyOptional } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class AdUnitRuleUpdateCmd {
  @ApiModelProperty()
  public id: number;

  @ApiModelProperty()
  public payload?: string;
}