import { ApiModelProperty, ApiModelPropertyOptional } from "@nestjs/swagger";

export class AdUnitRuleClearCmd {
  @ApiModelProperty() adUnitId: number;
  @ApiModelPropertyOptional() userId?: number;
}