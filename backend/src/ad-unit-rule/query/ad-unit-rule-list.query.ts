import { ApiModelProperty, ApiModelPropertyOptional } from "@nestjs/swagger";

export class AdUnitRuleListQuery {
  @ApiModelPropertyOptional() adUnitId?: number;
  @ApiModelPropertyOptional() userId?: number;
}