import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { User } from "../user/user.entity";
import { AdUnit } from "../ad-unit/ad-unit.entity";

@Entity()
export class AdUnitRule {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => AdUnit, adUnit => adUnit.rules, { onDelete: 'CASCADE' })
  adUnit: AdUnit;

  @ManyToOne(type => User, user => user.adUnitRules, { onDelete: 'CASCADE' })
  user: User;

  @Column({ default: "{}" })
  payload: string;
}
