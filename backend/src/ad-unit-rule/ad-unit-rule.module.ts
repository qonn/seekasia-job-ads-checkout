import { Module } from '@nestjs/common';
import { AdUnitRuleService } from './ad-unit-rule.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdUnitRule } from './ad-unit-rule.entity';

@Module({
  imports: [TypeOrmModule.forFeature([AdUnitRule])],
  providers: [AdUnitRuleService],
  exports: [AdUnitRuleService]
})
export class AdUnitRuleModule {}
