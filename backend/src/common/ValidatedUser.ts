export interface ValidatedUser {
  id: number;
  username: string;
}