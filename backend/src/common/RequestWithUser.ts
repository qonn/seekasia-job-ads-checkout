import { ValidatedUser } from "./ValidatedUser";

export interface RequestWithValidatedUser extends Request {
  user: ValidatedUser;
}