import 'dotenv/config';

import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();
  app.setGlobalPrefix('api');

  const options = new DocumentBuilder()
    .setBasePath('api')
    .setTitle('SEEK Asia Job Ads Checkout')
    .setDescription('API for the mass')
    .setVersion('0.1')
    .addBearerAuth('Authorization', 'header', 'apiKey')
    .addTag('Users')
    .addTag('Authentication')
    .addTag('Ad Units')
    .addTag('Checkout')
    .build();

  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup('api', app, document);

  app.useGlobalPipes(new ValidationPipe());

  await app.listen(3000);
}

bootstrap();
