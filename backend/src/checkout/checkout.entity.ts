import { PrimaryGeneratedColumn, ManyToOne, Entity, OneToMany, Column } from "typeorm";
import { User } from "../user/user.entity";
import { CheckoutItem } from "./checkout-item.entity";

@Entity()
export class Checkout {
  @PrimaryGeneratedColumn()
  public id: number;

  @ManyToOne(_ => User, user => user.checkouts)
  public user: User;

  @OneToMany(_ => CheckoutItem, checkoutItem => checkoutItem.checkout, { onDelete: 'CASCADE' })
  public items: CheckoutItem[];

  @Column()
  public paid: boolean;

  @Column()
  public total: number;
}