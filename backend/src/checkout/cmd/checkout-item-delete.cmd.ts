import { ApiModelProperty } from "@nestjs/swagger";

export class CheckoutItemDeleteCmd {
  @ApiModelProperty()
  public itemId: number;
}