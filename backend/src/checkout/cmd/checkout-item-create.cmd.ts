import { ApiModelProperty } from "@nestjs/swagger";

export class CheckoutItemCreateCmd {
  @ApiModelProperty()
  public adUnitId: number;
}