import { Controller, Post, UseGuards, Delete, Get, Req, Body, Param } from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { CheckoutService } from './checkout.service';
import { RequestWithValidatedUser } from '../common/RequestWithUser';
import { CheckoutItemCreateCmd } from './cmd/checkout-item-create.cmd';
import { CheckoutItemDeleteCmd } from './cmd/checkout-item-delete.cmd';

@ApiBearerAuth()
@ApiUseTags('Checkout')
@Controller('checkout')
export class CheckoutController {
  constructor(private readonly checkoutService: CheckoutService) {}
  /**
   * POST /api/checkout
   */
  @Post()
  @UseGuards(AuthGuard('jwt'))
  public async createItem(@Req() req: RequestWithValidatedUser, @Body() cmd: CheckoutItemCreateCmd) {
    return this.checkoutService.createItem(req.user.id, cmd);
  }

  /**
   * DELETE /api/checkout
   */
  @Delete()
  @UseGuards(AuthGuard('jwt'))
  public async clearItems(@Req() req: RequestWithValidatedUser) {
    return this.checkoutService.clearItems(req.user.id);
  }

  /**
   * DELETE /api/checkout/:itemId
   */
  @Delete(':itemId')
  @UseGuards(AuthGuard('jwt'))
  public async deleteItem(@Req() req: RequestWithValidatedUser, @Param() cmd: CheckoutItemDeleteCmd) {
    return this.checkoutService.deleteItem(req.user.id, cmd);
  }

  /**
   * GET /api/checkout
   */
  @Get()
  @UseGuards(AuthGuard('jwt'))
  public async getActiveCheckout(@Req() req: RequestWithValidatedUser) {
    return this.checkoutService.getActiveCheckout(req.user.id);
  }
}
