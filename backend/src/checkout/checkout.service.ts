import {
  Injectable,
  BadRequestException,
  UnauthorizedException,
} from '@nestjs/common';
import { Checkout } from './checkout.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CheckoutItem } from './checkout-item.entity';
import { RequestWithValidatedUser } from '../common/RequestWithUser';
import { CheckoutItemCreateCmd } from './cmd/checkout-item-create.cmd';
import { AdUnit } from '../ad-unit/ad-unit.entity';
import { AdUnitService } from '../ad-unit/ad-unit.service';
import { CheckoutItemDeleteCmd } from './cmd/checkout-item-delete.cmd';
import { UserCheckoutDto } from './dto/user-checkout.dto';
import { UserCheckoutItemDto } from './dto/user-checkout-item.dto';
import { AdUnitRuleService } from '../ad-unit-rule/ad-unit-rule.service';
import { CheckoutModifier } from './checkout.modifier';

@Injectable()
export class CheckoutService {
  constructor(
    @InjectRepository(Checkout)
    private readonly checkoutRepository: Repository<Checkout>,
    @InjectRepository(CheckoutItem)
    private readonly checkoutItemRepository: Repository<CheckoutItem>,
    private readonly adUnitService: AdUnitService,
    private readonly adUnitRuleService: AdUnitRuleService
  ) {}

  async createItem(userId: number, cmd: CheckoutItemCreateCmd) {
    let checkout: Checkout = await this.checkoutRepository.findOne({
      where: { user: { id: userId } },
      relations: ['items', 'items.adUnit'],
    });

    if (!checkout) {
      checkout = await this.checkoutRepository.save({
        user: { id: userId },
        paid: false,
        total: 0,
      });
    }

    let adUnit: AdUnit = await this.adUnitService.getById(cmd.adUnitId);


    if (!adUnit) {
      return 0;
    }

    let item: CheckoutItem = await this.checkoutItemRepository.save({
      adUnit: adUnit,
      checkout: checkout,
      originalPrice: adUnit.price,
      modifiedPrice: adUnit.price,
    });

    let adUnitRules = await this.adUnitRuleService.findByUserId(userId);

    if (!checkout.items) {
      checkout.items = [];
    }

    checkout.items = [...checkout.items, item];

    let modifiedItems = await CheckoutModifier.calculate(checkout, adUnitRules);

    if (modifiedItems.length) {
      await this.checkoutItemRepository.save(modifiedItems);
    }

    await this.checkoutRepository.save(checkout);

    return item.id;
  }

  async deleteItem(userId: number, cmd: CheckoutItemDeleteCmd) {
    let checkoutItem = await this.checkoutItemRepository.findOne(cmd.itemId, {
      relations: ['checkout', 'checkout.user'],
    });

    if (!checkoutItem) {
      throw new BadRequestException('Unable to find checkout item.');
    }

    if (
      checkoutItem.checkout &&
      checkoutItem.checkout.user &&
      checkoutItem.checkout.user.id !== userId
    ) {
      throw new UnauthorizedException();
    }

    await this.checkoutItemRepository.remove([checkoutItem]);

    let checkout: Checkout = await this.getUserActiveCheckout(userId);
    let adUnitRules = await this.adUnitRuleService.findByUserId(userId);
    let modifiedItems = await CheckoutModifier.calculate(checkout, adUnitRules);

    if (modifiedItems.length) {
      await this.checkoutItemRepository.save(modifiedItems);
    }

    await this.checkoutRepository.save(checkout);

    return;
  }

  async getActiveCheckout(userId: number) {
    let checkout: Checkout = await this.getUserActiveCheckout(userId);

    let checkoutDto: UserCheckoutDto = {
      items: checkout.items.map<UserCheckoutItemDto>(i => ({
        id: i.id,
        adUnitName: i.adUnit.name,
        originalPrice: i.originalPrice,
        modifiedPrice: i.modifiedPrice,
      })),
      total: checkout.total
    };

    return checkoutDto;
  }

  private async getUserActiveCheckout(userId: number) {
    let checkout: Checkout = await this.checkoutRepository.findOne({
      where: { user: { id: userId }, paid: false },
      relations: ['items', 'items.adUnit'],
    });

    if (!checkout) {
      checkout = await this.checkoutRepository.save({
        user: { id: userId },
        paid: false,
        total: 0,
      });
    }

    if (!checkout.items) {
      checkout.items = [];
    }

    return checkout;
  }

  async clearItems(userId: number) {
    let checkout = await this.getUserActiveCheckout(userId);
    await this.checkoutItemRepository.delete({ checkout: { id: checkout.id } });
    return;
  }

  async truncate() {
    await this.checkoutItemRepository.clear();
    await this.checkoutRepository.clear();
  }
}
