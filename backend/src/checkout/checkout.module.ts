import { Module } from '@nestjs/common';
import { CheckoutController } from './checkout.controller';
import { CheckoutService } from './checkout.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Checkout } from './checkout.entity';
import { CheckoutItem } from './checkout-item.entity';
import { AdUnitModule } from '../ad-unit/ad-unit.module';
import { UserModule } from '../user/user.module';
import { AdUnitRuleModule } from '../ad-unit-rule/ad-unit-rule.module';

@Module({
  imports: [UserModule, AdUnitModule, AdUnitRuleModule, TypeOrmModule.forFeature([Checkout, CheckoutItem])],
  controllers: [CheckoutController],
  providers: [CheckoutService]
})
export class CheckoutModule {}
