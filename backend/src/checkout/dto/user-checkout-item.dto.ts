export class UserCheckoutItemDto {
  public id: number;
  public adUnitName: string;
  public originalPrice: number;
  public modifiedPrice: number;
}