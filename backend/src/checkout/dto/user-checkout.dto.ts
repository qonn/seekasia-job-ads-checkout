import { UserCheckoutItemDto } from "./user-checkout-item.dto";

export class UserCheckoutDto {
  public items: UserCheckoutItemDto[];
  public total: number;
}