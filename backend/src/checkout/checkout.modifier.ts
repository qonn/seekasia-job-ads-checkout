import { Checkout } from './checkout.entity';
import { AdUnitRule } from '../ad-unit-rule/ad-unit-rule.entity';
import { CheckoutItem } from './checkout-item.entity';
import { Repository } from 'typeorm';
import { tsThisType } from '@babel/types';

interface ModifierCommonPayload {
  cmd: string;
}

interface XForYDealPayload {
  x: number;
  y: number;
}

interface SetPricePayload {
  to: number;
}

interface SetPriceOnMoreThanXAmountPurchasedPayload {
  x: number;
  to: number;
}

export class CheckoutModifier {
  private modifiedItems: CheckoutItem[] = [];

  constructor(private checkout: Checkout, private adUnitRules: AdUnitRule[]) {}

  public static calculate(checkout: Checkout, adUnitRules: AdUnitRule[]) {
    let cm = new CheckoutModifier(checkout, adUnitRules);
    return cm.execute();
  }

  public execute() {
    for (let adUnitRule of this.adUnitRules) {
      if (!adUnitRule.payload) {
        adUnitRule.payload = '{}';
      }

      let parsedAdUnitRule: ModifierCommonPayload = JSON.parse(
        adUnitRule.payload,
      );

      if (
        this[parsedAdUnitRule.cmd] &&
        typeof this[parsedAdUnitRule.cmd] === 'function'
      ) {
        let items = this.checkout.items.filter(i => i.adUnit.id === adUnitRule.adUnit.id);
        this[parsedAdUnitRule.cmd](items, parsedAdUnitRule);
      }
    }

    if (this.checkout.items && this.checkout.items.length) {
      this.checkout.total = this.checkout.items
        .map(i => i.modifiedPrice)
        .reduce((a, b) => a + b);
    }

    return this.modifiedItems;
  }

  public xForYDeal(items: CheckoutItem[], payload: XForYDealPayload) {
    if (!items.length) return;

    let originalPrice = items[0].originalPrice;
    let applyFromI = 0;

    for (let i = 0; i < items.length; i++) {
      let indexPlus1 = i + 1;
      let shouldApplyDeal = indexPlus1 % payload.x;

      if (shouldApplyDeal !== 0) {
        continue;
      }

      let newCalculatedPrice = (originalPrice * payload.y) / payload.x;

      let willBeAppliedItems = items.slice(applyFromI, applyFromI + payload.x);

      for (let willBeAppliedItem of willBeAppliedItems) {
        willBeAppliedItem.modifiedPrice = newCalculatedPrice;
      }

      this.modifiedItems = [...this.modifiedItems, ...willBeAppliedItems];

      applyFromI = i + 1;
    }

    return;
  }

  public async setPrice(items: CheckoutItem[], payload: SetPricePayload) {
    if (!items.length) return;

    for (let item of items) {
      item.modifiedPrice = payload.to;
    }

    this.modifiedItems = [...this.modifiedItems, ...items];
  }

  public async setPriceOnMoreThanXAmountPurchased(
    items: CheckoutItem[],
    payload: SetPriceOnMoreThanXAmountPurchasedPayload,
  ) {
    if (items.length < payload.x) return;
    this.setPrice(items, payload);
  }
}
