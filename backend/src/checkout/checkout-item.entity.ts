import { Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn, Column } from "typeorm";
import { Checkout } from "./checkout.entity";
import { AdUnit } from "../ad-unit/ad-unit.entity";

@Entity()
export class CheckoutItem {
  @PrimaryGeneratedColumn()
  public id: number;

  @ManyToOne(_ => Checkout, checkout => checkout.items, { onDelete: 'CASCADE' })
  public checkout: Checkout;

  @ManyToOne(_ => AdUnit, undefined, { onDelete: 'SET NULL' })
  public adUnit: AdUnit;

  @Column()
  public originalPrice: number;

  @Column()
  public modifiedPrice: number;
}