import { Checkout } from './checkout.entity';
import { AdUnit } from '../ad-unit/ad-unit.entity';
import { User } from '../user/user.entity';
import { AdUnitRule } from '../ad-unit-rule/ad-unit-rule.entity';
import { CheckoutItem } from './checkout-item.entity';
import { CheckoutModifier } from './checkout.modifier';

describe('CheckoutModifier', () => {
  let userDefault: User = {
    id: 1,
    username: 'test',
    password: 'test',
    adUnitRules: [],
    checkouts: [],
  };

  let userUnilever: User = {
    id: 2,
    username: 'unilever',
    password: 'test',
    adUnitRules: [],
    checkouts: [],
  };

  let userApple: User = {
    id: 3,
    username: 'apple',
    password: 'test',
    adUnitRules: [],
    checkouts: [],
  };

  let userNike: User = {
    id: 4,
    username: 'nike',
    password: 'test',
    adUnitRules: [],
    checkouts: [],
  };

  let userFord: User = {
    id: 5,
    username: 'ford',
    password: 'test',
    adUnitRules: [],
    checkouts: [],
  };

  let adUnitClassic: AdUnit = {
    id: 1,
    adId: 'classic',
    name: 'Classic Ad',
    price: 269.99,
    rules: [],
  };

  let adUnitStandout: AdUnit = {
    id: 2,
    adId: 'standout',
    name: 'Standout Ad',
    price: 322.99,
    rules: [],
  };

  let adUnitPremium: AdUnit = {
    id: 3,
    adId: 'premium',
    name: 'Premium Ad',
    price: 394.99,
    rules: [],
  };

  let adUnitRuleUnilever: AdUnitRule = {
    id: 1,
    adUnit: adUnitClassic,
    payload: `{ "cmd": "xForYDeal", "x": "3", "y": "2" }`,
    user: userUnilever,
  };

  let adUnitRuleApple: AdUnitRule = {
    id: 2,
    adUnit: adUnitStandout,
    payload: `{ "cmd": "setPrice", "to": 299.99 }`,
    user: userApple,
  };

  let adUnitRuleNike: AdUnitRule = {
    id: 3,
    adUnit: adUnitPremium,
    payload: `{ "cmd": "setPriceOnMoreThanXAmountPurchased", "x": "4", "to": 379.99 }`,
    user: userNike,
  };

  let adUnitRulesFord: AdUnitRule[] = [
    {
      id: 4,
      adUnit: adUnitClassic,
      payload: `{ "cmd": "xForYDeal", "x": "5", "y": "4" }`,
      user: userFord,
    },
    {
      id: 5,
      adUnit: adUnitStandout,
      payload: `{ "cmd": "setPrice", "to": 309.99 }`,
      user: userFord,
    },
    {
      id: 6,
      adUnit: adUnitPremium,
      payload: `{ "cmd": "setPriceOnMoreThanXAmountPurchased", "x": "3", "to": 389.99 }`,
      user: userFord,
    },
  ];

  userDefault.adUnitRules = [];

  userUnilever.adUnitRules = [adUnitRuleUnilever];
  userApple.adUnitRules = [adUnitRuleApple];
  userNike.adUnitRules = [adUnitRuleNike];
  userFord.adUnitRules = adUnitRulesFord;

  adUnitClassic.rules = [adUnitRuleUnilever];
  adUnitStandout.rules = [adUnitRuleApple, adUnitRulesFord[1]];
  adUnitPremium.rules = [adUnitRuleNike, adUnitRulesFord[2]];

  function generateCheckoutItems(checkout: Checkout, whichAd: AdUnit, howMuch: number) {
    let lastIndex =
      checkout && checkout.items && checkout.items.length
        ? checkout.items[checkout.items.length - 1].id
        : 1;

    for (let i = 0; i < howMuch; i++) {
      lastIndex++;

      checkout.items.push({
        id: lastIndex,
        adUnit: whichAd,
        checkout,
        originalPrice: whichAd.price,
        modifiedPrice: whichAd.price,
      })
    }
  }

  it('should no break on empty checkout', async () => {
    let checkout: Checkout = {
      id: 1,
      items: [],
      paid: false,
      total: 0,
      user: userDefault,
    };

    let modifiedItems = CheckoutModifier.calculate(
      checkout,
      userDefault.adUnitRules,
    );

    expect(modifiedItems.length).toBe(0);
    expect(checkout.total).toBe(0);
  });

  it('should be able to handle scenario #1', async () => {
    let checkout: Checkout = {
      id: 1,
      items: [],
      paid: false,
      total: 0,
      user: userDefault,
    };

    let checkoutItem: CheckoutItem = {
      id: 1,
      adUnit: adUnitClassic,
      checkout,
      originalPrice: adUnitClassic.price,
      modifiedPrice: adUnitClassic.price,
    };

    let checkoutItem2: CheckoutItem = {
      id: 2,
      adUnit: adUnitStandout,
      checkout,
      originalPrice: adUnitStandout.price,
      modifiedPrice: adUnitStandout.price,
    };

    let checkoutItem3: CheckoutItem = {
      id: 3,
      adUnit: adUnitPremium,
      checkout,
      originalPrice: adUnitPremium.price,
      modifiedPrice: adUnitPremium.price,
    };

    checkout.items = [checkoutItem, checkoutItem2, checkoutItem3];

    let modifiedItems = CheckoutModifier.calculate(
      checkout,
      checkout.user.adUnitRules,
    );

    expect(modifiedItems.length).toBe(0);
    expect(checkout.total).toBe(987.97);
  });

  it('should be able to handle scenario #2', async () => {
    let checkout: Checkout = {
      id: 1,
      items: [],
      paid: false,
      total: 0,
      user: userUnilever,
    };

    let checkoutItem: CheckoutItem = {
      id: 1,
      adUnit: adUnitClassic,
      checkout,
      originalPrice: adUnitClassic.price,
      modifiedPrice: adUnitClassic.price,
    };

    let checkoutItem2: CheckoutItem = {
      id: 2,
      adUnit: adUnitClassic,
      checkout,
      originalPrice: adUnitClassic.price,
      modifiedPrice: adUnitClassic.price,
    };

    let checkoutItem3: CheckoutItem = {
      id: 3,
      adUnit: adUnitClassic,
      checkout,
      originalPrice: adUnitClassic.price,
      modifiedPrice: adUnitClassic.price,
    };

    let checkoutItem4: CheckoutItem = {
      id: 4,
      adUnit: adUnitPremium,
      checkout,
      originalPrice: adUnitPremium.price,
      modifiedPrice: adUnitPremium.price,
    };

    checkout.items = [
      checkoutItem,
      checkoutItem2,
      checkoutItem3,
      checkoutItem4,
    ];

    let modifiedItems = CheckoutModifier.calculate(
      checkout,
      checkout.user.adUnitRules,
    );

    expect(modifiedItems.length).toBe(3);
    expect(checkout.total).toBe(934.97);
  });

  it('should be able to handle scenario #3', async () => {
    let checkout: Checkout = {
      id: 1,
      items: [],
      paid: false,
      total: 0,
      user: userApple,
    };

    checkout.items = [
      {
        id: 1,
        adUnit: adUnitStandout,
        checkout,
        originalPrice: adUnitStandout.price,
        modifiedPrice: adUnitStandout.price,
      },
      {
        id: 2,
        adUnit: adUnitStandout,
        checkout,
        originalPrice: adUnitStandout.price,
        modifiedPrice: adUnitStandout.price,
      },
      {
        id: 3,
        adUnit: adUnitStandout,
        checkout,
        originalPrice: adUnitStandout.price,
        modifiedPrice: adUnitStandout.price,
      },
      {
        id: 4,
        adUnit: adUnitPremium,
        checkout,
        originalPrice: adUnitPremium.price,
        modifiedPrice: adUnitPremium.price,
      },
    ];

    let modifiedItems = CheckoutModifier.calculate(
      checkout,
      checkout.user.adUnitRules,
    );

    expect(modifiedItems.length).toBe(3);
    expect(checkout.total).toBe(1294.96);
  });

  it('should be able to handle scenario #4', async () => {
    let checkout: Checkout = {
      id: 1,
      items: [],
      paid: false,
      total: 0,
      user: userNike,
    };

    checkout.items = [
      {
        id: 1,
        adUnit: adUnitPremium,
        checkout,
        originalPrice: adUnitPremium.price,
        modifiedPrice: adUnitPremium.price,
      },
      {
        id: 2,
        adUnit: adUnitPremium,
        checkout,
        originalPrice: adUnitPremium.price,
        modifiedPrice: adUnitPremium.price,
      },
      {
        id: 3,
        adUnit: adUnitPremium,
        checkout,
        originalPrice: adUnitPremium.price,
        modifiedPrice: adUnitPremium.price,
      },
      {
        id: 4,
        adUnit: adUnitPremium,
        checkout,
        originalPrice: adUnitPremium.price,
        modifiedPrice: adUnitPremium.price,
      },
    ];

    let modifiedItems = CheckoutModifier.calculate(
      checkout,
      checkout.user.adUnitRules,
    );

    expect(modifiedItems.length).toBe(4);
    expect(checkout.total).toBe(1519.96);
  });

  it('should be able to handle scenario #5', async () => {
    let checkout: Checkout = {
      id: 1,
      items: [],
      paid: false,
      total: 0,
      user: userFord,
    };

    generateCheckoutItems(checkout, adUnitClassic, 5);
    generateCheckoutItems(checkout, adUnitStandout, 5);
    generateCheckoutItems(checkout, adUnitPremium, 5);

    let modifiedItems = CheckoutModifier.calculate(
      checkout,
      checkout.user.adUnitRules,
    );

    expect(modifiedItems.length).toBe(15);
    expect(checkout.total.toFixed(2)).toBe('4579.86');
  });
});
