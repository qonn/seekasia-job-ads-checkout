import { Controller, Post, Get, UseGuards, Request, HttpCode } from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth, ApiImplicitBody, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { AuthLoginCommand } from './cmd/auth-login.command';

@ApiUseTags('Authentication')
@ApiBearerAuth()
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  @HttpCode(200)
  @UseGuards(AuthGuard('local'))
  @ApiOperation({ title: "login" })
  @ApiImplicitBody({ name: 'AuthLoginCmd', type: AuthLoginCommand })
  @ApiResponse({ status: 200, description: 'Successfully logged in.' })
  @ApiResponse({ status: 401, description: 'Login failed.' })
  login(@Request() req): Promise<any> {
    return this.authService.login(req.user);
  }
}
