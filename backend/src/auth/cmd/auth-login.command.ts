import { ApiModelProperty } from "@nestjs/swagger";

export class AuthLoginCommand {
  @ApiModelProperty() username: string;
  @ApiModelProperty() password: string;
}

