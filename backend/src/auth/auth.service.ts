import * as bcrypt from 'bcrypt';

import { Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import { AuthLoggedInDto } from './dto/auth-logged-in.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService
  ) {}

  public async validateUser(username: string, password: string): Promise<any> {
    const user = await this.userService.findOne(username);

    if (user && await bcrypt.compare(password, user.password)) {
      const { password, ...result } = user;
      return result;
    }

    return undefined;
  }

  async validateUserById(userId: number) {
    const user = await this.userService.getById(userId);

    if (user) {
      return true;
    }

    return false;
  }

  public async login(user: any) {
    const payload = {
      username: user.username,
      sub: user.id
    };

    return new AuthLoggedInDto({ accessToken: this.jwtService.sign(payload) })
  }
}
