import { ApiModelProperty } from "@nestjs/swagger";

export class AuthLoggedInDto {
  @ApiModelProperty() accessToken: string;

  constructor(data: { accessToken?: string }) {
    this.accessToken = data.accessToken;
  }
}