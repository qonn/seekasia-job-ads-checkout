import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { AdUnitModule } from './ad-unit/ad-unit.module';
import { AdUnitRuleModule } from './ad-unit-rule/ad-unit-rule.module';
import { CheckoutModule } from './checkout/checkout.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    UserModule,
    AuthModule,
    AdUnitModule,
    AdUnitRuleModule,
    CheckoutModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
