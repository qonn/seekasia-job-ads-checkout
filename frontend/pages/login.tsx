import MainLayout from "../components/MainLayout";
import fetch from 'isomorphic-unfetch';
import React, { ChangeEvent } from 'react'
import { setCookie } from 'nookies'
import Router from "next/router";

interface Props {
}

interface State {
  username: string,
  password: string
}

export default class Login extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      username: '',
      password: ''
    }
  }

  handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    const target = e.target as HTMLInputElement;
    const value = target.value;
    const key = target.name as keyof State;

    this.setState((prevState) => ({
      ...prevState,
      [key]: value,
    }));
  }

  handleLogin = async () => {
    const {
      username,
      password
    } = this.state;

    const res = await fetch('http://localhost:3000/api/auth/login', {
      method: 'POST',
      headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
      },
      body: JSON.stringify({
        username,
        password
      })
    });

    const data = await res.json();

    setCookie({}, 'accessToken', data.accessToken, {});

    Router.push('/');
  }

  render() {
    return <MainLayout>
      <div className="backdrop">
        <div className="loginBox">
          <h1>Job Ads Checkout</h1>

          <input name="username" type="text" placeholder="Username" value={this.state.username} onChange={this.handleInputChange} />
          <input name="password" type="password" placeholder="Password" value={this.state.password} onChange={this.handleInputChange} />

          <button className="login-button" onClick={this.handleLogin}>Login</button>
        </div>
      </div>

      <style jsx>{`
        .backdrop {
          background: #3d3d3d;
          width: 100vw;
          height: 100vh;
          display: flex;
          justify-content: center;
          align-items: center;
        }

        .loginBox {
          width: 500px;
          background: white;
          display: flex;
          flex-direction: column;
          justify-content: stretch;
          border-radius: 0.5em;
        }

        .loginBox h1 {
          text-align: center;
          padding: 1em 0;
        }

        .loginBox input {
          font-size: 1.5em;
          padding: 1em;
          border: 2px solid grey;
          margin: 0 0.75em 0.75em 0.75em;
        }

        .login-button {
          border: 2px solid #444;
          background: #f5f5f5;
          font-size: 1.5em;
          padding: 1em;
          margin: 0 0.75em 0.75em 0.75em;
          cursor: pointer;
        }

        .login-button:hover {
          background: #400000;
          border-color: #400000;
          color: white;
        }

        .login-button:active {
        }
      `}</style>
    </MainLayout>
  }
}