import { NextPage } from 'next';
import { withAuth } from '../hoc/withAuth';
import MainLayout from '../components/MainLayout';
import SiteLinks from '../components/SiteLinks';
import { parseCookies } from 'nookies';
import { AdUnitsApiFactory, CheckoutApiFactory } from '../codegen/api';
import { useToasts } from 'react-toast-notifications';

interface AdUnitListItem {
  id: number;
  adId: string;
  name: string;
  price: number;
}

interface Props {
  data: AdUnitListItem[]
}

const AdUnit: NextPage<Props> = (props) => {
  const { addToast } = useToasts()

  const onAddToCartClick = async (item: AdUnitListItem) => {
    let cookies = parseCookies();

    let checkoutApi = CheckoutApiFactory({
      apiKey: `bearer ${cookies.accessToken}`,
    }, undefined, 'http://localhost:3000/api');

    try {
      let res = await checkoutApi.checkoutPost({ adUnitId: item.id });
    } catch (e) {
    }

    addToast('Item added to cart', {
      appearance: 'success',
      autoDismiss: true,
      pauseOnHover: false,
    });
  }

  return (
    <MainLayout>
      <div className="backdrop">
        <div className="wrapper">
          <SiteLinks />

          <div className="adUnitBox">
            <h1>Available Ad Units</h1>
            { props.data.length ? props.data.map(m => <div key={m.id} className="adUnitItem">
              <div className="adUnitName">{ m.name }</div>
              <div className="adUnitPrice">{ `\$${m.price.toFixed(2)}` }</div>
              <button className="addToCartButton" onClick={() => onAddToCartClick(m)}>
                <svg viewBox="0 0 24 24">
                  <path d="M21.381 5.345c-0.191-0.219-0.466-0.345-0.756-0.345h-13.819l-0.195-1.164c-0.080-0.482-0.497-0.836-0.986-0.836h-2.25c-0.553 0-1 0.447-1 1s0.447 1 1 1h1.403l1.86 11.164c0.008 0.045 0.031 0.082 0.045 0.124 0.016 0.053 0.029 0.103 0.054 0.151 0.032 0.066 0.075 0.122 0.12 0.179 0.031 0.039 0.059 0.078 0.095 0.112 0.058 0.054 0.125 0.092 0.193 0.13 0.038 0.021 0.071 0.049 0.112 0.065 0.116 0.047 0.238 0.075 0.367 0.075 0.001 0 11.001 0 11.001 0 0.553 0 1-0.447 1-1s-0.447-1-1-1h-10.153l-0.166-1h11.319c0.498 0 0.92-0.366 0.99-0.858l1-7c0.041-0.288-0.045-0.579-0.234-0.797zM19.472 7l-0.285 2h-3.562v-2h3.847zM14.625 7v2h-3v-2h3zM14.625 10v2h-3v-2h3zM10.625 7v2h-3c-0.053 0-0.101 0.015-0.148 0.030l-0.338-2.030h3.486zM7.639 10h2.986v2h-2.653l-0.333-2zM15.625 12v-2h3.418l-0.285 2h-3.133z"></path>
                  <path d="M10.625 19.5c0 0.828-0.672 1.5-1.5 1.5s-1.5-0.672-1.5-1.5c0-0.828 0.672-1.5 1.5-1.5s1.5 0.672 1.5 1.5z"></path>
                  <path d="M19.625 19.5c0 0.828-0.672 1.5-1.5 1.5s-1.5-0.672-1.5-1.5c0-0.828 0.672-1.5 1.5-1.5s1.5 0.672 1.5 1.5z"></path>
                </svg>
              </button>
            </div>) : <div className="empty">No items</div> }
          </div>
        </div>

        <style jsx>{`
          .backdrop {
            background: #3d3d3d;
            width: 100vw;
            height: 100vh;
            display: flex;
            justify-content: center;
            align-items: center;
          }

          .wrapper ul {
            list-style: none;
            display: flex;
          }

          .wrapper li {
            color: white;
            text-align: center;
            padding: 0.25em 0.5em;
            margin-bottom: 1em;
            border-right: 1px solid white;
          }

          .wrapper li:last-child {
            border-right: 0;
          }

          .wrapper li a {
            color: white;
            text-decoration: none;
          }

          .wrapper li a:hover {
            color: white;
            text-decoration: underline;
          }

          .adUnitBox {
            width: 500px;
            background: white;
            display: flex;
            flex-direction: column;
            justify-content: stretch;
            border-radius: 0.5em;
          }

          .adUnitBox h1 {
            text-align: center;
            padding: 1em 0;
          }

          .adUnitBox .adUnitItem {
            padding: 1em;
            margin-bottom: 0.5em;
            border-top: 1px solid #ddd;
            border-bottom: 1px solid #ddd;
            text-align: center;
            display: flex;
            align-items: center;
          }

          .adUnitBox .adUnitName {
            font-weight: bold;
          }
          .adUnitBox .adUnitPrice {
            margin-left: auto;
          }

          .adUnitBox .empty {
            padding: 1em;
            margin-bottom: 0.5em;
            border-top: 1px solid #ddd;
            border-bottom: 1px solid #ddd;
            text-align: center;
          }

          .addToCartButton {
            border: 0;
            background: transparent;
            margin-left: 0.5em;
            font-size: 1.5em;
            padding: 0.25em;
            display: flex;
            align-items: center;
            justify-content: center;
            cursor: pointer;
            border-radius: 999px;
          }

          .addToCartButton:hover {
            background: #eee;
          }

          .addToCartButton:active {
            background: #ddd;
          }

          .addToCartButton svg {
            width: 1em;
            height: 1em;
            fill: currentColor;
          }
        `}</style>

      </div>
    </MainLayout>
  );
};

AdUnit.getInitialProps = async (ctx) => {
  let cookies = parseCookies(ctx);

  let adUnitsApi = AdUnitsApiFactory({
    apiKey: `bearer ${cookies.accessToken}`,
  }, undefined, 'http://localhost:3000/api');

  try {
   let res = await adUnitsApi.adUnitsGet();
   return await res.json();
  } catch (e) {
  }

  return {
    data: []
  };
};

export default withAuth(AdUnit);