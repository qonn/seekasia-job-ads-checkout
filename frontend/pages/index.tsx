import { NextPage } from 'next';
import { withAuth } from '../hoc/withAuth';
import MainLayout from '../components/MainLayout';
import { UsersApi, CheckoutApi, CheckoutApiFactory } from '../codegen/api';
import { parseCookies } from 'nookies';
import Link from 'next/link';
import SiteLinks from '../components/SiteLinks';
import { useToasts } from 'react-toast-notifications';
import Router from 'next/router';

interface CheckoutItem {
  id: number,
  adUnitName: string,
  originalPrice: number,
  modifiedPrice: number
}

interface Props {
  items: CheckoutItem[],
  total: number
}

const Home: NextPage<Props> = (props) => {
  const { addToast } = useToasts()

  const onRemoveFromCartClick = async (itemId: number) => {
    let cookies = parseCookies();

    let checkoutApi = CheckoutApiFactory({
      apiKey: `bearer ${cookies.accessToken}`,
    }, undefined, 'http://localhost:3000/api');

    try {
      let res = await checkoutApi.checkoutItemIdDelete(itemId);
    } catch (e) {
    }

    addToast('Item removed from cart', {
      appearance: 'success',
      autoDismiss: true,
      pauseOnHover: false,
    });

    Router.push('/');
  }

  return (
    <MainLayout>
      <div className="backdrop">
        <div className="wrapper">
          <SiteLinks />

          <div className="checkoutBox">
            <h1>My Checkout</h1>
            <div className="scroller">
              { props.items.length ? props.items.map(i => (
                <div className="checkoutItem">
                  <div className="checkoutItemName">{ i.adUnitName }</div>
                  <div className="checkoutItemPrice">{
                    i.originalPrice === i.modifiedPrice ? `\$${i.originalPrice.toFixed(2)}` :
                    (
                      <div>
                        <span className="original">{ `\$${i.originalPrice.toFixed(2)}` }</span>
                        <span className="modified">{ ` \$${i.modifiedPrice.toFixed(2)}` }</span>
                      </div>
                    ) }</div>
                  <button className="removeFromCartButton" onClick={() => onRemoveFromCartClick(i.id) }>
                    <svg viewBox="0 0 32 32">
                      <path d="M6 32h20l2-22h-24zM20 4v-4h-8v4h-10v6l2-2h24l2 2v-6h-10zM18 4h-4v-2h4v2z"></path>
                    </svg>
                  </button>
                </div>
              )) : <div className="empty">No items</div> }
            </div>
            <div className="total">Total: <span className="totalPrice">${ props.total.toFixed(2) }</span></div>
          </div>
        </div>

        <style jsx>{`
          .backdrop {
            background: #3d3d3d;
            width: 100vw;
            height: 100vh;
            display: flex;
            justify-content: center;
            align-items: center;
          }

          .checkoutBox {
            width: 500px;
            background: white;
            display: flex;
            flex-direction: column;
            justify-content: stretch;
            border-radius: 0.5em;
          }

          .checkoutBox h1 {
            text-align: center;
            padding: 1em 0;
          }

          .checkoutBox .empty {
            padding: 1em;
            margin-bottom: 0.5em;
            border-top: 1px solid #ddd;
            border-bottom: 1px solid #ddd;
            text-align: center;
          }

          .checkoutItem {
              padding: 1em;
              margin-bottom: 0.5em;
              border-top: 1px solid #ddd;
              border-bottom: 1px solid #ddd;
              text-align: center;
              display: flex;
              align-items: center;
            }

            .checkoutItemName {
              font-weight: bold;
            }
            .checkoutItemPrice {
              margin-left: auto;
            }

            .checkoutItemPrice .original {
              text-decoration: line-through;
              color: grey;
              font-size: 0.75em;
            }

            .checkoutItemPrice .modified {
              font-size: 1.25em;
              color: red;
            }

            .total {
              text-align: center;
              padding: 1em;
              margin-bottom:0.5em;
              font-weight: bold;
              font-size: 1.5em;
            }

            .totalPrice {
              font-weight: normal;
            }

            .scroller {
              max-height: 300px;
              overflow-y: auto;
            }

            .removeFromCartButton {
              border: 0;
              background: transparent;
              margin-left: 0.5em;
              font-size: 1.25em;
              padding: 0.5em;
              display: flex;
              align-items: center;
              justify-content: center;
              cursor: pointer;
              border-radius: 999px;
            }

            .removeFromCartButton:hover {
              background: #eee;
            }

            .removeFromCartButton:active {
              background: #ddd;
            }

            .removeFromCartButton svg {
              width: 1em;
              height: 1em;
              fill: currentColor;
            }
        `}</style>

      </div>
    </MainLayout>
  );
}

Home.getInitialProps = async (ctx) => {
  let cookies = parseCookies(ctx);

  let checkoutApi = CheckoutApiFactory({
    apiKey: `bearer ${cookies.accessToken}`,
  }, undefined, 'http://localhost:3000/api');

  try {
   let res = await checkoutApi.checkoutGet();
   return await res.json();
  } catch (e) {
  }

  return {
    items: [],
    total: 0
  };
};

export default withAuth(Home);