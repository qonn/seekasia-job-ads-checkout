import { Component, Router } from "react";
import { parseCookies } from "nookies";
import fetch from 'isomorphic-unfetch';

function redirectToLogin(ctx) {
    if (ctx.res) {
      ctx.res.writeHead(302, {
        Location: '/login'
      })
      ctx.res.end()
    } else {
      Router.push('/login')
    }

    return {};
}

export function withAuth(Page) {
  return class Authenticated extends Component {
    constructor(props) {
      super(props);
    }

    static async getInitialProps(ctx) {
      const cookies = parseCookies(ctx);

      if (!cookies.accessToken) {
        return redirectToLogin(ctx);
      }

      const res = await fetch('http://localhost:3000/api/users/me', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': `bearer ${cookies.accessToken}`
        }
      });

      if (res.status !== 200) {
        return redirectToLogin(ctx);
      }

      if (Page.getInitialProps)
        return Page.getInitialProps(ctx)

      return {};
    }

    render() {
      return <Page {...this.props} />
    }
  }
}

