import Link from 'next/link';

const SiteLinks = () => (
  <ul>
    <li><Link href="/"><a>My Checkout</a></Link></li>
    <li><Link href="/ad-units"><a>Available Ad Units</a></Link></li>
    <li><Link href="/login"><a>Logout</a></Link></li>

    <style jsx>{`
      ul {
        list-style: none;
        display: flex;
      }

      li {
        color: white;
        text-align: center;
        padding: 0.25em 0.5em;
        margin-bottom: 1em;
        border-left: 1px solid white;
      }

      li:first-child {
        border-left: 0
      }

      li:last-child {
        border-left: 0;
        margin-left: auto;
      }

      li a {
        color: white;
        text-decoration: none;
      }

      li a:hover {
        color: white;
        text-decoration: underline;
      }
    `}</style>
  </ul>
);

export default SiteLinks;