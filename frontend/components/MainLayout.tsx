import React from 'react'

export default class MainLayout extends React.Component {
  render() {
    return <div>
      {this.props.children}
      <style jsx global>{`
        @import url('https://fonts.googleapis.com/css?family=Roboto');
        * {
          box-sizing: border-box;
          margin: 0;
          padding: 0;
        }

        html {
          font-family: Roboto;
        }
      `}</style>
    </div>
  }
};